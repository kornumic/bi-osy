#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <string>
#include <utility>
#include <vector>
#include <array>
#include <iterator>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <stdexcept>
#include <condition_variable>
#include <pthread.h>
#include <semaphore.h>
#include "progtest_solver.h"
#include "sample_tester.h"
using namespace std;
#endif /* __PROGTEST__ */ 

//----------------------------------------------------------------------------------------------------------------------
class CompanyWrapper;
//-------------------------------ProgtestSolverWrapper----------------------------------------------------------------------
class ProblemPackWrapper
{
public:
    ProblemPackWrapper(AProblemPack pack, CompanyWrapper* company)
        : m_pack(std::move(pack)), m_parentCompany(company){
        if(m_pack){
            m_size = m_pack->m_Problems.size();
        } else{
            m_size = 0;
        }
    };

    AProblemPack m_pack;
    CompanyWrapper* m_parentCompany;
    size_t m_idx = 0;
    size_t m_size = 0;
    size_t m_solved = 0;

    /**
     * If number of solved problems equals to number of all problems in pack, pack is solved
     * @return true if pack is solved
     */
    bool isSolved() const{
        cout << "m_size " << m_size << " m_solved = " << m_solved <<endl;
        return m_size == m_solved;
    }

    /**
     * Should be always asked before calling getProblem()
     * Checks if index is a valid position im pack
     * @return true if getProblem() call would return valid pointer
     */
    bool canGetProblem() const{
        cout << "idx = " << m_idx << " m_size = " << m_size << endl;
        return m_idx < m_size;
    }

    /**
     * Used to obtain AProblem from problem pack.
     * Shouldn't be called without checking canGetProblem() - to stay in bounds.
     * Number of times this method was called should be stored and after solving problems, number of solved problems
     * should be set (if you call getProblem n times, after solving them all, problemsSolved(n) )
     * Unchecked calling, when idx is out of bounds throws exception
     * @return next unsolved problem.
     */
    AProblem getProblem(){
        if(m_idx >= m_size)
            throw std::invalid_argument("More idx is out of bounds");

        auto problem = m_pack->m_Problems[m_idx];
        m_idx++;
        return problem;
    }

    /**
     * Adds n to m_solved and checks if in bounds
     * If number of solved is invalid after, throws exception
     * @param n
     */
    void problemsSolved(size_t n){
        m_solved += n;
        if(m_solved > m_size)
            throw std::invalid_argument("More solved than obtained!");
    }
};

//-------------------------------ProgtestSolverWrapper----------------------------------------------------------------------
class ProgtestSolverWrapper
{
public:
    ProgtestSolverWrapper(){
        solver = createProgtestSolver();
    }
    bool m_isFilled = false;
    AProgtestSolver solver;
    //packs and number of its problems in solver
    std::map<shared_ptr<ProblemPackWrapper>, size_t> m_packsInSolver;   //WARNING this might not work with shared_ptr
};

//--------------------------------------Shared--------------------------------------------------------------------------
class Shared
{
public:
    std::condition_variable m_cond;
    std::mutex m_packMutex; // mutex to protect m_packBuffer and m_solverBuffer
    std::queue<ProblemPackWrapper> m_packBuffer;
    std::mutex m_solverMutex; // mutex to protect m_packBuffer and m_solverBuffer
    std::queue<ProgtestSolverWrapper> m_solverBuffer;

    std::mutex m_varsMutex;
    atomic_size_t m_companiesN = 0;
    atomic_size_t m_receivedAllPacks = 0;  //number of companies that have received all their packs
    atomic_size_t m_solvedAllPacks = 0;        //number of companies that have solved all their packs
};

//----------------------------------CompanyWrapper----------------------------------------------------------------------
class CompanyWrapper
{
public:
    explicit CompanyWrapper(ACompany& company, Shared& shared, int companyNumber)
        : m_company(company), m_shared(shared), m_companyNumber(companyNumber){}

    ACompany& m_company;
    Shared& m_shared;

    std::mutex m_orderMtx;
    std::queue<ProblemPackWrapper> m_packsOrder;

    thread waiter;
    thread returner;

    void commWait();        //thread
    void commReturn();      //thread

    std::condition_variable m_condReceived;
    size_t m_companyNumber;

    size_t m_receivedPacks = 0; // company counter that counts received packs
    size_t m_returnedPacks = 0; // company counter that counts returned packs
    bool m_receivedAll = false;
};

void CompanyWrapper::commWait() {
    while(true){
        AProblemPack pack = m_company->waitForPack();
        if(pack == nullptr){    //All packs are received
            unique_lock lockVariables(m_shared.m_varsMutex);
            m_shared.m_receivedAllPacks++;
            lockVariables.unlock();
            m_receivedAll = true;
            m_condReceived.notify_one();

            std::cout << "Received nullptr, all packs for company " << m_companyNumber << " received. Total( "<< m_shared.m_receivedAllPacks << "/" << m_shared.m_companiesN << ")"<< std::endl;
            if(m_shared.m_receivedAllPacks == m_shared.m_companiesN){
                std::cout << "All companies received all packs." << std::endl;
                // if number of received is equal to number of companies, all companies received all packs
                auto tmp = ProblemPackWrapper(nullptr, this);
                std::unique_lock lock(m_shared.m_packMutex);
                std::unique_lock lockOrder(m_orderMtx);
                m_shared.m_packBuffer.emplace(tmp);
                m_packsOrder.emplace(tmp);
                lockOrder.unlock();
                lock.unlock();

                m_shared.m_cond.notify_all();
            }
            return;  // wait thread ends
        }
        m_receivedPacks++;
        std::cout << "Company " << m_companyNumber << " received ProblemPack (total " << m_receivedPacks << ")" << std::endl;

        std::unique_lock lock(m_shared.m_packMutex);
        m_shared.m_packBuffer.emplace(pack, this);
        lock.unlock();
        m_shared.m_cond.notify_all();  // using m_cond to signal other threads that there is new problemPack

        m_packsOrder.emplace(pack, this);    //Add m_pack to packsOrder
        std::cout << "Company " << m_companyNumber << " added pack" << std::endl;
    }
}

void CompanyWrapper::commReturn() {
    while(true){
        cout << "Worker received all " << m_receivedAll << endl;
        if(m_receivedAll && (m_packsOrder.empty() || m_packsOrder.front().m_pack == nullptr) ){
            std::cout << "Company " << m_companyNumber << "has received and sent all problem packs!" << std::endl;
            break;
        }
        std::unique_lock lock(m_orderMtx);
        cout << "Company " << m_companyNumber << " waiting for solved problems" << endl;
        m_condReceived.wait(lock, [this] () {
            return !m_packsOrder.empty() && m_packsOrder.front().isSolved();
        });
        if(m_packsOrder.front().m_pack == nullptr){
            cout << "Closing commReturn thread for company " << m_companyNumber << endl;
            m_shared.m_solvedAllPacks++;
            return;
        }
        cout << "packsOrder is not empty!" << endl;

        while(true){
            if(m_packsOrder.empty() || !m_packsOrder.front().isSolved()){
                break;
            }
            auto tmp = m_packsOrder.front();
            m_packsOrder.pop();
            lock.unlock();
            cout << "SOLVING" << endl;
            m_company->solvedPack(tmp.m_pack);  //allow working with packsOrder while waiting for solvedPack
            m_returnedPacks++;
            lock.lock();
        }
        //all problems from the front of the queue were solved
        lock.unlock();

    }
}

//----------------------------------COptimizer--------------------------------------------------------------------------
class COptimizer {
public:     // Dummy methods for progtest interface
    static bool usingProgtestSolver(void)
    {
        return true;
    }
    static void checkAlgorithm(AProblem problem)
    {
        // dummy implementation if usingProgtestSolver() returns true
    }
public:     // Required interface for progtest; to implement
    void start(int threadCount);
    void stop(void);
    void addCompany(ACompany company);

private:
    void worker(); // Does all the hard worker
private:
    std::vector<std::shared_ptr<CompanyWrapper>> m_companies;
    std::vector<std::thread> m_workThreads;

    Shared m_shared;
    // private member variables
    bool hasStarted = false;
};

void COptimizer::worker() {
    size_t workerNumber = m_workThreads.size() + 1;
    std::cout << "Worker " << workerNumber << " spawned." << std::endl;
    while(true){
        std::cout << "Worker " << workerNumber << " waiting for a work." << std::endl;
        std::unique_lock<std::mutex> lockPack(m_shared.m_packMutex);
        m_shared.m_cond.wait(lockPack, [this] () {
            return !m_shared.m_packBuffer.empty();
        });
        if(m_shared.m_packBuffer.front().m_pack == nullptr){
            std::cout << "Worker " << workerNumber << " ending." << std::endl;
            m_shared.m_cond.notify_all();
            return;
        }
        std::unique_lock<std::mutex> lockSolver(m_shared.m_solverMutex);
        if(m_shared.m_solverBuffer.empty()){
            m_shared.m_solverBuffer.emplace();
        }
        auto solverW = m_shared.m_solverBuffer.front();

        //loops until solver is filled
        while(solverW.solver->hasFreeCapacity()){
            std::cout << "Worker " << workerNumber << " working." << std::endl;
            // now it has not empty packBuffer
            auto packW = make_shared<ProblemPackWrapper>(m_shared.m_packBuffer.front());
            if(packW->canGetProblem()){
                cout << "got problem" << endl;
                auto problem = m_shared.m_packBuffer.front().getProblem();
                solverW.solver->addProblem(problem);
                solverW.m_packsInSolver[packW]++;
                if(!solverW.solver->hasFreeCapacity()){
                    solverW.m_isFilled = true;  //after adding problem to solver its capacity is filled, can solve now
                    break;
                }
            }
            else{   //couldn't get new problem from pack, solver is not filled and we need new packs
                cout << "cant get problem, popping" << endl;
                m_shared.m_packBuffer.pop();
                if(solverW.solver->hasFreeCapacity()){
                    lockSolver.unlock();
                    lockPack.unlock();
                }
                break;
            }
        }
        if(!solverW.m_isFilled){
            cout << "Solver was not filled by pack, continue." << endl;
            m_shared.m_cond.notify_all();
            continue;
        }
        cout << "Solver was filled by worker " << workerNumber << endl;
        m_shared.m_solverBuffer.pop();
        lockSolver.unlock();
        lockPack.unlock();

        cout << "Worker " << workerNumber << " working." << endl;
        size_t solved = solverW.solver->solve();

        cout << "Worker " << workerNumber << " solved " << solved << "problems" << endl;
        for(auto& i : solverW.m_packsInSolver){
            i.first->problemsSolved(i.second);
            cout <<"solved = " << i.first->m_solved << endl;
            if(i.first->isSolved()){
                i.first->m_solved = true;
                i.first->m_parentCompany->m_condReceived.notify_one();
            }
        }
    }
}

void COptimizer::start(int threadCount) {
    if(hasStarted)
        throw std::invalid_argument("This COptimizer has already started!");
    hasStarted = true;

    // start workers
    for(int i = 0; i < threadCount; i++){
        m_workThreads.emplace_back(&COptimizer::worker, this);
    }

    // start communication threads for all companies
    for(auto & company : m_companies){
        company->waiter = std::thread(&CompanyWrapper::commWait, company);
        company->returner = std::thread(&CompanyWrapper::commReturn, company);
    }
}

void COptimizer::stop(void) {
    for(const auto& company : m_companies){
        company->waiter.join();
    }
    cout << "Joined all waiters" << endl;

    for(auto &i : m_workThreads){
        i.join();
    }
    cout << "Joined all workers" << endl;

    for(const auto& company : m_companies){
        company->returner.join();
    }
    cout << "Joined all returners" << endl;

}

void COptimizer::addCompany(ACompany company) {
    std::shared_ptr<CompanyWrapper> ptr = std::make_shared<CompanyWrapper>(company, m_shared, m_companies.size() + 1);
    m_companies.push_back(ptr);
    m_shared.m_companiesN++;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int                                    main                                    ( void )
{
  COptimizer optimizer;
  ACompanyTest  company = std::make_shared<CCompanyTest> ();
  optimizer . addCompany ( company );
  optimizer . start ( 4 );
  optimizer . stop  ();
  if ( ! company -> allProcessed () )
    throw std::logic_error ( "(some) problems were not correctly processsed" );
  return 0;  
}
#endif /* __PROGTEST__ */ 
