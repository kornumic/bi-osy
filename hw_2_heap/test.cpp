#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <iostream>
#include <utility>
#include <vector>

using namespace std;
#endif /* __PROGTEST__ */

#define NULL_OFFSET (uint32_t)(-1) //offset representing nullptr in memory
#define MIN_POW 5
#define MAX_POW 31
#define USED_BLOCK 0xFF
#define FREE_BLOCK 0x00

void* g_memPool = nullptr;
uint32_t g_memSize = 0;
uint32_t g_freeBlocks[MAX_POW + 1];  //starts of linked lists of free blocks with  size = 2^i
uint8_t g_maxBlock = 0;     //biggest block in memory
uint32_t g_allocated = 0;
bool g_initialized = false;


/**
 * Fast power of 2 with shifting bytes
 * @param power
 * @return power of 2
 */
uint32_t power(uint8_t power){
    uint32_t res = 1;
    return (res << power);
}
/**
 * Returns power of provided base
 * Slower than bite shifting
 * @param base 
 * @param power 
 * @return power of base
 */
uint32_t power(uint32_t base, uint32_t pow){
    if(base == 2)
        return power(pow);
    uint32_t res = 1;
    for(uint32_t i = 0; i < pow; i++)
        res *= base;
    return res;
}
/**
 * Sets maximum block by current state of g_freeBlocks
 * Max block is set as biggest index in g_freeBlocks that is not null
 * Should be only used in the beginning right after memory init
 */
void setMaxBlock(){
    g_maxBlock = 0;
    for(int i = 0; i < MAX_POW; i++){
        if(g_freeBlocks[i] != NULL_OFFSET){
            g_maxBlock = i;
        }
    }
}
/**
 * Sets all cells in g_freeBlocks as null
 */
void clearFreeBlocks(){
    for(unsigned int & g_freeBlock : g_freeBlocks){
        g_freeBlock = NULL_OFFSET;
    }
}

/**
 * Calculates logarithm to size of block that can contain requested memory in allocation and header
 * @param sizeWithHeader
 * @return log(blocks size)
 */
uint8_t calculateRequiredPow(uint32_t sizeWithHeader) {
    uint8_t pow = MIN_POW;
    uint32_t blockSize = power(pow);
    while(blockSize < sizeWithHeader && blockSize != 0){
        pow++;
        blockSize = power(pow);
    }
    return pow;
}
/**
 * Creates void* to memory pool from byte offset
 * @param byteOffset - offset of bytes from beginning of
 * @return nullptr, pointer to memory pool
 */
void* offsetToPtr(uint32_t byteOffset){
    if(byteOffset == NULL_OFFSET)
        return nullptr;
    return reinterpret_cast<void*>(reinterpret_cast<uint8_t*>(g_memPool) + byteOffset);
}
/**
 * Creates offset from pointer to memory pool
 * @param ptr
 * @return NULL_OFFSET, offset from start of memory pool
 */
uint32_t ptrToOffset(void* ptr){
    if(ptr == nullptr){
        return NULL_OFFSET;
    }
    return reinterpret_cast<uint8_t*>(ptr) - reinterpret_cast<uint8_t*>(g_memPool);
}

struct Header {
    Header(uint8_t power, uint8_t used, uint32_t arena, uint16_t spacer,
           uint32_t next = NULL_OFFSET, uint32_t prev = NULL_OFFSET)
        : m_power(power), m_used(used), m_arena(arena), m_spacer(spacer), m_next(next), m_prev(prev){};
    explicit Header(uint8_t power)
        : m_power(power), m_used(FREE_BLOCK), m_arena(0), m_spacer(0), m_next(NULL_OFFSET), m_prev(NULL_OFFSET){};

    /**
     * Writes header attributes to memory to provided ptr
     * There needs to be enough space in memory from pos to write successfully
     * @param pos
     * @return void
     */
    void write(void* ptr) const;
    /**
     * Writes header attributes to memory to provided offset
     * There needs to be enough space in memory from pos to write successfully
     * @param pos
     * @return void
     */
    void write(uint32_t offset) const;
    /**
     * Reads header attributes from memory at provided ptr
     * There needs to be enough space in memory from pos to read successfully
     * @param pos
     * @return header that was on provided ptr
     */
    static Header read(void* ptr);
    /**
     * Reads header attributes from memory at provided offset
     * There needs to be enough space in memory from pos to read successfully
     * @param pos
     * @return header that was on provided offset
     */
    static Header read(uint32_t offset);

    void free();

    void clear();
    const static uint8_t size = 16;
    uint8_t  m_power;   // 1B 2**power (all blocks are multiples of 2)
    uint8_t  m_used;    // 1B
    uint32_t m_arena;   // 4B
    uint16_t m_spacer;  // 2B
    uint32_t m_next;    // 4B
    uint32_t m_prev;    // 4B
};                      // 16B = sum, size of block

void Header::write(void* ptr) const {
    this->write(ptrToOffset(ptr));
}

void Header::write(uint32_t offset) const {
    auto power = reinterpret_cast<uint8_t*>(offsetToPtr(offset));
    *power = this->m_power;
    offset += sizeof(this->m_power);
    auto used = reinterpret_cast<uint8_t*>(offsetToPtr(offset));
    *used = this->m_used;
    offset += sizeof(this->m_used);
    auto arena = reinterpret_cast<uint32_t*>(offsetToPtr(offset));
    *arena = this->m_arena;
    offset += sizeof(this->m_arena);
    auto spacer = reinterpret_cast<uint16_t*>(offsetToPtr(offset));
    *spacer = this->m_spacer;
    offset += sizeof(this->m_spacer);
    auto next = reinterpret_cast<uint32_t*>(offsetToPtr(offset));
    *next = this->m_next;
    offset += sizeof(this->m_next);
    auto prev = reinterpret_cast<uint32_t*>(offsetToPtr(offset));
    *prev = this->m_prev;
}

Header Header::read(void* ptr){
    return Header::read(ptrToOffset(ptr));
}

Header Header::read(uint32_t offset){
    auto power = reinterpret_cast<uint8_t*>(offsetToPtr(offset));
    offset += sizeof(uint8_t);
    auto used = reinterpret_cast<uint8_t*>(offsetToPtr(offset));
    offset += sizeof(uint8_t);
    auto arena = reinterpret_cast<uint32_t*>(offsetToPtr(offset));
    offset += sizeof(uint32_t);
    auto spacer = reinterpret_cast<uint16_t*>(offsetToPtr(offset));
    offset += sizeof(uint16_t);
    auto next = reinterpret_cast<uint32_t*>(offsetToPtr(offset));
    offset += sizeof(uint32_t);
    auto prev = reinterpret_cast<uint32_t*>(offsetToPtr(offset));
    return {*power, *used, *arena, *spacer, *next, *prev};
}

void Header::free() {
    this->m_used = FREE_BLOCK;
    this->m_arena = 0;
    this->m_spacer = 0;
    this->m_next = NULL_OFFSET;
    this->m_prev = NULL_OFFSET;
}

void Header::clear(){
    this->m_power = 0;
    this->m_used = 0;
    this->m_arena = 0;
    this->m_spacer = 0;
    this->m_next = 0;
    this->m_prev = 0;
}

void printLinkedList(uint8_t i) {
//    cout << "at power " << (uint32_t) i << ":" << endl;
    uint32_t offset = g_freeBlocks[i];
    while(offset != NULL_OFFSET){
//        cout << offset << " - offset, " << offsetToPtr(offset) << " ptr" << endl;
        auto header = Header::read(offset);
        offset = header.m_next;
    }
}

void printFreeBlocks() {
    for(uint8_t i = MIN_POW; i <= g_maxBlock; i++){
        printLinkedList(i);
    }
//    cout<<endl;
}
/**
 * First it cleans free blocks. Initializes memory by splitting it to blocks in power of two with minimum size.
 * Fills g_freeBlocks with offsets to these blocks.
 * Sets memSize to size covered by free blocks.
 * In the end sets boundary for maximum block size.
 */
void initMemory(){
    clearFreeBlocks();

    uint8_t pow = MAX_POW;
    uint32_t offset = 0;
    for(uint8_t i = 0; i <= MAX_POW - MIN_POW; i++){
        uint32_t powered = power(pow);
        uint32_t masked = powered & g_memSize;
        if(masked != 0){ //a bit on position of mask (which is power of 2) is 1
            auto header = Header(pow);
            header.write(offset);
            g_freeBlocks[pow] = offset;
            offset += masked;
        }
        pow--;
    }
    g_memSize = offset;
    setMaxBlock();
}
/**
 * Checks if heap can be initialized and if yes, initializes it.
 * Splits memory to blocks that are covering as much memory as possible.
 * Sets flag g_initialized as true.
 * @param memPool
 * @param memSize
 */
void HeapInit(void* memPool, int memSize)
{
    if(g_initialized){
        return;
    }
    if(memPool == nullptr || (memSize < (int)power(MIN_POW))){
        g_memPool = nullptr;
        g_memSize = 0;
        g_initialized = false;
        return;
    }
    g_memPool = memPool;
    g_memSize = memSize;

    memset(g_memPool, 0, g_memSize);
    initMemory();
//    printFreeBlocks();
    g_initialized = true;
}
/**
 * Removes header from free blocks linked list.
 * @param header
 */
void removeFromFreeBlocks(Header& header){
    if( header.m_next == NULL_OFFSET &&
        header.m_prev == NULL_OFFSET &&
        g_freeBlocks[header.m_power] == NULL_OFFSET){
        return;
    }
    if(header.m_next != NULL_OFFSET){
        auto next = Header::read(header.m_next);
        next.m_prev = header.m_prev;
        next.write(header.m_next);
    }
    if(header.m_prev != NULL_OFFSET){
        auto prev = Header::read(header.m_prev);
        prev.m_next = header.m_next;
        prev.write(header.m_prev);
    }
    else{
        //previous is null, so it is first in the list
        g_freeBlocks[header.m_power] = header.m_next;
    }
    header.m_next = NULL_OFFSET;
    header.m_prev = NULL_OFFSET;
}
/**
 * Allocates memory to free block at offset.
 * Sets header attributes and then writes them to the memory
 * @param offset
 * @param size
 * @return pointer to allocated arena
 */
void* allocate(uint32_t offset, int size) {
    auto header = Header::read(offset);
    if(header.m_used != FREE_BLOCK){
        return nullptr;
    }
    removeFromFreeBlocks(header);
    //set header values to mark it as allocated
    header.m_used = USED_BLOCK;
    header.m_arena = size;
    //update header in memory
    header.write(offset);
    return offsetToPtr(offset + Header::size);
}

/**
 * Pushes header to front of freeBlocks saves it in the memory
 * @param header
 * @param headerOffset
 */
void pushFrontFreeBlocks(Header& header, uint32_t headerOffset){
    auto nextOffset = g_freeBlocks[header.m_power];
    if(nextOffset == NULL_OFFSET){
        //there is nothing in freeBlock cell, simply push
        g_freeBlocks[header.m_power] = headerOffset;
        header.m_next = NULL_OFFSET;
        header.m_prev = NULL_OFFSET;
        header.write(headerOffset);
        return;
    }
    //list is not empty, set it as first and attach next
    auto next = Header::read(nextOffset);
    next.m_prev = headerOffset;
    header.m_next = nextOffset;
    header.m_prev = NULL_OFFSET;
    g_freeBlocks[header.m_power] = headerOffset;
    //save to the memory
    header.write(headerOffset);
    next.write(nextOffset);
}
/**
 * Splits block that is on provided offset. Adds second block to the freeBlocks and
 * writes both new blocks to the memory.
 * @param offset
 * @return
 */
uint32_t splitBlock(uint32_t offset) {
    auto block = Header::read(offset);
    removeFromFreeBlocks(block);
    uint8_t pow = block.m_power - 1;
    auto otherBlock = Header(pow);
    pushFrontFreeBlocks(otherBlock, offset + power(pow));
    block.m_power = pow;
    block.m_used = FREE_BLOCK;
    pushFrontFreeBlocks(block, offset);
    return offset;
}
/**
 * If there is a free block in this power of freeBlocks, splits it and returns offset of the
 * first block (the one with smaller offset). If no block is present, calls itself again, but with higher required block
 * Recursive call returns NULL_OFFSET when there is no memory left - required block is bigger than available block of maximum size
 * @param requiredBlockPower
 * @return NULL_OFFSET - out of memory,
 *         offset of first free block if there enough memory
 */
uint32_t splitBiggerBlock(uint8_t requiredBlockPower) {
    uint8_t myBlockPower = requiredBlockPower + 1;
    if(myBlockPower > g_maxBlock)
        return NULL_OFFSET; //out of memory

    uint32_t blockToSplitOffset = g_freeBlocks[myBlockPower];
    if(blockToSplitOffset == NULL_OFFSET){
        //there is no block I can split in my freeBlocks cell, request split of bigger freeBlock
        blockToSplitOffset = splitBiggerBlock(myBlockPower);
    }
    if(blockToSplitOffset == NULL_OFFSET){
        return NULL_OFFSET; //there is no block that can be split, OUT OF MEMORY
    }
    //I have a block I can split
    auto firstBlockOff = splitBlock(blockToSplitOffset);
//    printFreeBlocks();
    return firstBlockOff;
}

void* HeapAlloc(int size)
{
    if(!g_initialized)
        return nullptr; //check if we are not trying to alloc to uninitialized memory

    uint8_t requiredBlockPower = calculateRequiredPow(size + (uint32_t) Header::size);
    void* p;
    auto offset = g_freeBlocks[requiredBlockPower];
    if(offset == NULL_OFFSET){
        auto blockOffset = splitBiggerBlock(requiredBlockPower);
        if(blockOffset == NULL_OFFSET)
            return nullptr;
        offset = blockOffset;
    }
    p = allocate(offset, size);
    g_allocated++;
    return p;
}
/**
 * Checks attributes and offset of header. If any of attributes does not match state allocated,
 * header is not valid/allocated
 * @param header
 * @param offset
 * @return true - all attributes are valid
 *         false - otherwise
 */
bool checkHeaderAllocated(Header header, uint32_t offset) {
    return !(
        offset % power(header.m_power) != 0 ||
        header.m_used != USED_BLOCK ||
        header.m_power < MIN_POW || header.m_power > MAX_POW ||
        header.m_next != NULL_OFFSET ||
        header.m_prev != NULL_OFFSET
    );
}
/**
 * Returns buddy offset in same power of 2 of some offset
 * If buddy does not exist (its position is not in memory pool) returns NULL_OFFSET
 * @param pow
 * @param byteOffset
 * @return NULL_OFFSET, buddy offset
 */
uint32_t findBuddyOff(uint8_t pow, uint32_t byteOffset){
    auto buddyOffset = power(pow) ^ byteOffset;
    auto offset = buddyOffset >= g_memSize ? NULL_OFFSET : buddyOffset;
    if(offset == NULL_OFFSET)
        return NULL_OFFSET;

    auto buddy = Header::read(offset);
    if(buddy.m_power == pow && buddy.m_used == FREE_BLOCK)
        return offset;
    return NULL_OFFSET;
}
/**
 * Merges two buddies, that are not connected to anybody and create one big block.
 * This block is then saved to the memory and returned;
 * @param buddy1
 * @param buddy2
 * @param merged
 * @return
 */
Header mergeBuddies(uint32_t buddy1, uint32_t buddy2, uint32_t &merged) {
    uint32_t smallerOff = buddy1 < buddy2 ? buddy1 : buddy2;
    uint32_t biggerOff = buddy1 > buddy2 ? buddy1 : buddy2;
    auto smaller = Header::read(smallerOff);
    auto bigger = Header::read(biggerOff);
    smaller.m_power += 1;
    bigger.clear();
    //write them to the memory
    smaller.write(smallerOff);
    bigger.write(biggerOff);
    merged = smallerOff;
    return smaller;
}
/**
 * Recursively merges free buddies, until there is no-buddy :)
 * If there is no free buddy, adds block to front of freeBlocks
 * @param header
 * @param headerOffset
 */
void mergeWithBuddy(Header& header, uint32_t headerOffset) {
    auto buddyOffset = findBuddyOff(header.m_power, headerOffset);
    if(buddyOffset == NULL_OFFSET){
        //doesn't have buddy, push him to front of corresponding freeBlocks cell
        pushFrontFreeBlocks(header, headerOffset);
        return;
    }
    //has buddy
    auto buddy = Header::read(buddyOffset);
    if(buddy.m_used == USED_BLOCK) {
        //buddy is used, cannot merge, add header to freeBlocks
        pushFrontFreeBlocks(header, headerOffset);
        return;
    }
    //buddy is free, can merge them
    removeFromFreeBlocks(buddy);
    buddy.m_next = NULL_OFFSET;
    buddy.m_prev = NULL_OFFSET;
    buddy.write(buddyOffset);
    //create one big block from two smaller buddies
    auto mergedOffset = NULL_OFFSET;
    Header merged = mergeBuddies(headerOffset, buddyOffset, mergedOffset);
    mergeWithBuddy(merged, mergedOffset);
}
/**
 * Function receives pointer. From it offset of header is calculated and if it's valid, header is read and checked.
 * If everything is valid, header is freed and merged with it's buddy, if it has one that is free.
 * @param blk
 * @return
 */
bool HeapFree(void* blk)
{
    if(!g_initialized)
        return false;
    auto headerOffset = ptrToOffset(blk) - Header::size;
    if(headerOffset > g_memSize)
        return false;
    auto header = Header::read(headerOffset);
    if(!checkHeaderAllocated(header, headerOffset))
        return false;

    header.free();
    header.write(headerOffset);
    mergeWithBuddy(header, headerOffset);
    g_allocated--;
    return true;
}
/**
 * Checks if heap is initialized. If not, does nothing, just returns pendingBlk = 0 .
 * Cleans heap, and g_* variables. Sets freeBlocks cells as null.
 * @param pendingBlk
 */
void HeapDone(int* pendingBlk)
{
    if(!g_initialized){
        *pendingBlk = 0;
        return;
    }
    *pendingBlk = (int)g_allocated;
    clearFreeBlocks();
    g_allocated = 0;
    g_memPool = nullptr;
    g_memSize = 0;
    g_maxBlock = 0;
    g_initialized = false;
}

#ifndef __PROGTEST__
int main ( void )
{
    uint8_t       * p0, *p1, *p2, *p3, *p4;
    int             pendingBlk;
    static uint8_t  memPool[3 * 1048576]; //3MB
    static uint8_t  memPool2[INT32_MAX];    //maximum


    uint8_t *q1, *q2, *q3, *q4, *q5, *q6;

    HeapInit( memPool, 1024);
    assert ( ( q1 = (uint8_t*) HeapAlloc ( 8 ) ) != NULL );
    memset( q1, 1, 8 );
    auto header = Header::read(ptrToOffset(q1) - Header::size );
    assert ( ( q2 = (uint8_t*) HeapAlloc ( 17 ) ) != NULL );
    memset( q2, 2, 17 );
    header = Header::read(ptrToOffset(q2) - Header::size );
    assert ( ( q3 = (uint8_t*) HeapAlloc ( 8 ) ) != NULL );
    memset( q3, 3, 8 );
    header = Header::read(ptrToOffset(q3) - Header::size );
    assert ( ( q4 = (uint8_t*) HeapAlloc ( 51 ) ) != NULL );
    memset( q4, 4, 51 );
    header = Header::read(ptrToOffset(q4) - Header::size );
    assert ( ( q5 = (uint8_t*) HeapAlloc ( 195 ) ) != NULL );
    memset( q5, 5, 195 );
    header = Header::read(ptrToOffset(q5) - Header::size );
    assert ( ( q6 = (uint8_t*) HeapAlloc ( 78 ) ) != NULL );
    memset( q6, 6, 78 );
    header = Header::read(ptrToOffset(q6) - Header::size );
    assert ( HeapFree ( q1 ) );
    assert ( HeapFree ( q2 ) );
    assert ( !HeapFree ( q2 ) );
    assert ( HeapFree ( q3 ) );
    assert ( HeapFree ( q4 ) );
    assert ( HeapFree ( q5 ) );
    assert ( HeapFree ( q6 ) );
    assert ( ( q4 = (uint8_t*) HeapAlloc ( 150 ) ) != NULL );
    memset( q4, 7, 150 );
    header = Header::read(ptrToOffset(q4) - Header::size );
    HeapDone( &pendingBlk );
    assert ( pendingBlk == 1 );

//    randomWithCheck();

    HeapInit( memPool2, INT32_MAX/2+1);
    assert ( ( p0 = (uint8_t*) HeapAlloc ( INT32_MAX/2+1 - 16 ) ) != NULL );
    memset( p0, 1, INT32_MAX/2+1 - 16 );
    assert ( ( p1 = (uint8_t*) HeapAlloc ( 1 ) ) == NULL );
    assert ( HeapFree ( p0 ) );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 0 );

    HeapInit( memPool, 256);
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 113 ) ) != NULL );
    memset( p0, 1, 113 );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 1 );

    HeapInit( memPool, 256);
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 240 ) ) != NULL );
    memset( p0, 1, 240 );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 1 );

    HeapInit ( memPool, 255);
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 90 ) ) != NULL );
    memset ( p0, 1, 90 );
    assert ( ( p1 = (uint8_t*) HeapAlloc ( 20 ) ) != NULL );
    memset ( p1, 2, 20 );
    assert ( ( p2 = (uint8_t*) HeapAlloc ( 5 ) ) != NULL );
    memset ( p2, 3, 5 );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 3 );

    HeapInit ( memPool, 255);
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 5 ) ) != NULL );
    memset ( p0, 1, 5 );
    assert ( ( p1 = (uint8_t*) HeapAlloc ( 5 ) ) != NULL );
    memset ( p1, 2, 5 );
    assert ( ( p2 = (uint8_t*) HeapAlloc ( 5 ) ) != NULL );
    memset ( p2, 3, 5 );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 3 );


    HeapInit ( memPool, 2097152 );
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 512000 ) ) != NULL );
    memset ( p0, 1, 512000 );
    assert ( ( p1 = (uint8_t*) HeapAlloc ( 511000 ) ) != NULL );
    memset ( p1, 2, 511000 );
    assert ( ( p2 = (uint8_t*) HeapAlloc ( 26000 ) ) != NULL );
    memset ( p2, 3, 26000 );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 3 );


    HeapInit ( memPool, 2097152 );
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 1000000 ) ) != NULL );
    memset ( p0, 1, 1000000 );
    assert ( ( p1 = (uint8_t*) HeapAlloc ( 250000 ) ) != NULL );
    memset ( p1, 2, 250000 );
    assert ( ( p2 = (uint8_t*) HeapAlloc ( 250000 ) ) != NULL );
    memset ( p2, 3, 250000 );
    assert ( ( p3 = (uint8_t*) HeapAlloc ( 250000 ) ) != NULL );
    memset ( p3, 4, 250000 );
    assert ( ( p4 = (uint8_t*) HeapAlloc ( 50000 ) ) != NULL );
    memset ( p4, 5, 50000 );
    assert ( HeapFree ( p2 ) );
    assert ( HeapFree ( p4 ) );
    assert ( HeapFree ( p3 ) );
    assert ( HeapFree ( p1 ) );
    assert ( ( p1 = (uint8_t*) HeapAlloc ( 500000 ) ) != NULL );
    memset ( p1, 1, 500000 );
    assert ( HeapFree ( p0 ) );
    assert ( HeapFree ( p1 ) );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 0 );


    HeapInit ( memPool, 2359296 );
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 1000000 ) ) != NULL );
    memset ( p0, 0, 1000000 );
    assert ( ( p1 = (uint8_t*) HeapAlloc ( 500000 ) ) != NULL );
    memset ( p1, 0, 500000 );
    assert ( ( p2 = (uint8_t*) HeapAlloc ( 500000 ) ) != NULL );
    memset ( p2, 0, 500000 );
    assert ( ( p3 = (uint8_t*) HeapAlloc ( 500000 ) ) == NULL );
    assert ( HeapFree ( p2 ) );
    assert ( ( p2 = (uint8_t*) HeapAlloc ( 300000 ) ) != NULL );
    memset ( p2, 0, 300000 );
    assert ( HeapFree ( p0 ) );
    assert ( HeapFree ( p1 ) );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 1 );


    HeapInit ( memPool, 2359296 );
    assert ( ( p0 = (uint8_t*) HeapAlloc ( 1000000 ) ) != NULL );
    memset ( p0, 0, 1000000 );
    assert ( ! HeapFree ( p0 + 1000 ) );
    HeapDone ( &pendingBlk );
    assert ( pendingBlk == 1 );


    return 0;
}
#endif /* __PROGTEST__ */

